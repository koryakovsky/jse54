package ru.nlmk.study.jse53.restfulapi.api;

import java.io.Serializable;

public class UserResponse implements Serializable {

    //serial

    private boolean success;
    private Long id;

    public UserResponse(boolean success, Long id) {
        this.success = success;
        this.id = id;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
