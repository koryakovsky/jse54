package ru.nlmk.study.jse53.restfulapi;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.nlmk.study.jse53.restfulapi.api.RegisterUserRequest;
import ru.nlmk.study.jse53.restfulapi.api.UserResponse;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserServiceImpl userServiceImpl;

    @GetMapping(value = "/", produces = "application/json")
    public List<User> getAllUsers() {
        return userServiceImpl.getAllUsers();
    }

    @GetMapping(value = "/xml", produces = "application/xml")
    public List<User> getAllUsersXml() {
        return userServiceImpl.getAllUsers();
    }

    @GetMapping(value = "/{id}/", produces = "application/json")
    public User getUserById(@PathVariable Long id) {
        return userServiceImpl.getUserById(id);
    }

    @GetMapping(value = "/{id}/xml", produces = "application/xml")
    public User getUserByIdXml(@PathVariable Long id) {
        return userServiceImpl.getUserById(id);
    }

    @GetMapping("/findByName")
    public User getUserByName(@RequestParam(required = false) String name) {
        return userServiceImpl.getUserByName(name);
    }

    @PostMapping("/")
    public ResponseEntity<UserResponse> registerUser(@RequestBody RegisterUserRequest request) {
        Long userId = userServiceImpl.registerUser(request);
        return ResponseEntity.status(201).body( new UserResponse(true, userId));
    }

    @DeleteMapping("/{id}/")
    public ResponseEntity<UserResponse> deleteUser(@PathVariable Long id) {
        Long deletedId = userServiceImpl.deleteUser(id);
        return ResponseEntity.status(200).body( new UserResponse(true, deletedId));
    }
}
