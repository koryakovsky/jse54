package ru.nlmk.study.jse53.restfulapi.api;

import java.io.Serializable;

public class RegisterUserRequest implements Serializable {

    // serial

    private String name;
    private String email;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
