package ru.nlmk.study.jse53.restfulapi;

import org.springframework.stereotype.Service;
import ru.nlmk.study.jse53.restfulapi.api.RegisterUserRequest;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

@Service
public class UserServiceImpl  {

    private static final List<User> users = new ArrayList<>();
    private final AtomicLong counter = new AtomicLong();

    {
        User user = new User();
        user.setId(counter.incrementAndGet());
        user.setName("Tomas");
        user.setEmail("tomas@mail.com");
        user.setCarModelName("Aston Martin");
        user.setRegDate(new Date());

        users.add(user);
    }

    public List<User> getAllUsers() {
        return users;
    }

    public User getUserById(Long id) {
        for (User user : users) {
            if (user.getId().equals(id)){
                return user;
            }
        }

        //грязный хак
        return null;
    }


    public User getUserByName(String name) {
        for (User user : users) {
            if (user.getName().equals(name)){
                return user;
            }
        }

        //грязный хак
        return null;
    }

    public Long registerUser(RegisterUserRequest request) {
        User user = new User();
        user.setId(counter.incrementAndGet());
        user.setName(request.getName());
        user.setEmail(request.getName());
        users.add(user);

        return user.getId();
    }

    public Long deleteUser(Long id){
        for (int i = 0; i < users.size(); i++) {
            if (users.get(i).getId().equals(id)){
                return users.remove(i).getId();
            }
        }

        //грязный хак
        return null;
    }
}
