package ru.nlmk.study.jse53.resttemplate.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public class CityLocation {

    @JsonProperty
    private String title;

    @JsonProperty("location_type")
    private LocationType locationType;

    @JsonProperty("woeid")
    private String cityId;

    @JsonProperty("latt_long")
    private String lattLong;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocationType getLocationType() {
        return locationType;
    }

    public void setLocationType(LocationType locationType) {
        this.locationType = locationType;
    }

    public String getCityId() {
        return cityId;
    }

    public void setCityId(String cityId) {
        this.cityId = cityId;
    }

    public String getLattLong() {
        return lattLong;
    }

    public void setLattLong(String lattLong) {
        this.lattLong = lattLong;
    }
}
