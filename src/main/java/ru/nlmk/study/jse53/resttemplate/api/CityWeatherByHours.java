package ru.nlmk.study.jse53.resttemplate.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class CityWeatherByHours {

    @JsonProperty("consolidated_weather")
    private List<CityWeather> cityWeatherList;

    public List<CityWeather> getCityWeatherList() {
        return cityWeatherList;
    }

    public void setCityWeatherList(List<CityWeather> cityWeatherList) {
        this.cityWeatherList = cityWeatherList;
    }
}
