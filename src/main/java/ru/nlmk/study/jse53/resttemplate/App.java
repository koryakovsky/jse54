package ru.nlmk.study.jse53.resttemplate;


import ru.nlmk.study.jse53.resttemplate.api.CityWeatherByHours;

public class App {

    private final static WeatherService weatherService = new WeatherService();

    public static void main(String[] args) {


        CityWeatherByHours moscow = weatherService.getCityWeather("berlin");

        System.out.println(moscow.getCityWeatherList().get(0));
    }
}
