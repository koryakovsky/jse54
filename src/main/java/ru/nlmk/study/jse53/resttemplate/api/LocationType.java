package ru.nlmk.study.jse53.resttemplate.api;

import com.fasterxml.jackson.annotation.JsonProperty;

public enum LocationType {

    @JsonProperty("City")
    CITY,

    @JsonProperty("Village")
    VILLAGE

}
