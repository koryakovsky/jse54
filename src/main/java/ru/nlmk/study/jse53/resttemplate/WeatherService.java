package ru.nlmk.study.jse53.resttemplate;

import org.springframework.web.client.RestTemplate;
import ru.nlmk.study.jse53.resttemplate.api.CityLocation;
import ru.nlmk.study.jse53.resttemplate.api.CityWeatherByHours;

public class WeatherService {

    private final static String WEATHER_API_URL = "https://www.metaweather.com/api/location/";

    private final RestTemplate restTemplate = new RestTemplate();

    public CityLocation[] searchCity(String city) {

        return doRequest(WEATHER_API_URL+ "search/?query=" + city, CityLocation[].class);
    }

    public CityWeatherByHours getCityWeather(String city) {
        System.out.println("Город: " + city);

        CityLocation[] cityLocations = searchCity(city);

        String cityId = cityLocations[0].getCityId();

        return doRequest(WEATHER_API_URL + cityId + "/", CityWeatherByHours.class);
    }

    private <T> T doRequest(String url, Class<T> rqType) {
        return restTemplate.getForEntity(url, rqType).getBody();
    }
}
