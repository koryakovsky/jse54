package ru.nlmk.study.jse53.resttemplate.api;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.Date;

public class CityWeather {

    private Long id;

    @JsonProperty("weather_state_name")
    private String weatherStateName;

    @JsonProperty("wind_direction")
    private String windDirection;

    @JsonProperty("applicable_date")
    private Date date;

    @JsonProperty("min_temp")
    private Double minTemp;

    @JsonProperty("max_temp")
    private Double maxTemp;

    @JsonProperty("the_temp")
    private Double actualTemp;

    @JsonProperty("wind_speed")
    private Double windSpeed;

    @JsonProperty("air_pressure")
    private Double airPressure;

    private Double humidity;

    private Double visibility;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getWeatherStateName() {
        return weatherStateName;
    }

    public void setWeatherStateName(String weatherStateName) {
        this.weatherStateName = weatherStateName;
    }

    public String getWindDirection() {
        return windDirection;
    }

    public void setWindDirection(String windDirection) {
        this.windDirection = windDirection;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getMinTemp() {
        return minTemp;
    }

    public void setMinTemp(Double minTemp) {
        this.minTemp = minTemp;
    }

    public Double getMaxTemp() {
        return maxTemp;
    }

    public void setMaxTemp(Double maxTemp) {
        this.maxTemp = maxTemp;
    }

    public Double getActualTemp() {
        return actualTemp;
    }

    public void setActualTemp(Double actualTemp) {
        this.actualTemp = actualTemp;
    }

    public Double getWindSpeed() {
        return windSpeed;
    }

    public void setWindSpeed(Double windSpeed) {
        this.windSpeed = windSpeed;
    }

    public Double getAirPressure() {
        return airPressure;
    }

    public void setAirPressure(Double airPressure) {
        this.airPressure = airPressure;
    }

    public Double getHumidity() {
        return humidity;
    }

    public void setHumidity(Double humidity) {
        this.humidity = humidity;
    }

    public Double getVisibility() {
        return visibility;
    }

    public void setVisibility(Double visibility) {
        this.visibility = visibility;
    }

    @Override
    public String toString() {
        return
                "Погодные условия: " + weatherStateName + "\n" +
                "Направление ветра: " + windDirection + "\n" +
                "Скорость ветра: " + windSpeed + "\n" +
                "Дата: " + date + "\n" +
                "Температура: " + actualTemp + "\n" +
                "Влажность: " + humidity + "\n" +
                "Давление воздуха: " + airPressure + "\n" +
                "Видимость: " + visibility;

    }
}
